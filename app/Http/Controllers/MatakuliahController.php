<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MatakuliahController extends Controller
{
      public function index() {
    	$data['result'] = \App\Matakuliah::all();
    	return view('matakuliah/index')->with($data);
    }

    	public function create() 
    	{
    		return view('matakuliah/form');
    	}

    	public function store(Request $request) {
  			$input	= $request->all();
  			$status = \App\Matakuliah::create($input);

  			return redirect('/');
    	}
}
