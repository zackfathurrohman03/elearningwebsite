<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matakuliah extends Model
{
       public $primaryKey ='id_matakuliah';

    protected $table ='t_matakuliah';

    protected $fillable = [
    	'nama_matakuliah', 'id_pengajar'];
}
