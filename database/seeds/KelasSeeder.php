<?php

use Illuminate\Database\Seeder;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
   		$data = [
   		'nama_kelas' => '3-SIP',
   		'jurusan' => 'Sistem Informasi Profesional'];
   		DB::table('t_kelas')->insert($data);
    }
}
