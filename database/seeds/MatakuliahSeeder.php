<?php

use Illuminate\Database\Seeder;

class MatakuliahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
   		'nama_matakuliah' => 'Praktek Pemrograman Web 2',
   		'id_pengajar' => '1'];
   		DB::table('t_matakuliah')->insert($data);

   		$data = [
   		'nama_matakuliah' => 'Teori Pemrograman Web 2',
   		'id_pengajar' => '2'];
   		DB::table('t_matakuliah')->insert($data);
    }
}
