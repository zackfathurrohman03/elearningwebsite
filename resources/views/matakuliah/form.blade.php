   @extends('templates/header')

   @section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Data Matakuliah
        <small>SMK Negeri 19 </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url ('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Data Matakuliah</li>
        <li class="active">Tambah Data Matakuliah</li>
      </ol>
    </section>

    <!-- Main content -->
   <div class="section">
  <form action="{{ url('/') }}" method="POST">
  {!! csrf_field() !!}
    <div class="form-goup">
              <label class="control-label col-sm-2">Nama Matakuliah</label>
              <div class="col-sm-10">
                <input type="text" name="nama_matakuliah" class="form-control" placeholder="Masukan Nama Matakuliah" />
                </div>
            </div>
            <div class="form-goup">
              <label class="control-label col-sm-2">id Pengajar</label>
              <div class="col-sm-10">
                <input type="text" name="id_pengajar" class="form-control" placeholder="Masukan ID Pengajar" />
                </div>
            </div>
              <div class="form-group">
              <div class="col-sm-10 col-sm-offset-2">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>Simpan</button>>
                </div>
            </div>

        </form>    
        </div>
        <!-- /.box-footer-->
        </div>
        <!-- /.box-footer-->
    </section>
    @endsection