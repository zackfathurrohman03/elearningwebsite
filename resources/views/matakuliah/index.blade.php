   @extends('templates/header')

   @section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Matakuliah
        <small>SMK Negeri 19 </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Matakuliah</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <a href="{{ url('matakuliah/add') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i>Tambah</a>
          </div>
          <div class="box-body">
            <table class="table table-stripped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Matakuliah</th>
                  <th>Pengajar</th>
                  <td>Action</td>
                </tr>
              </thead>
              <tbody>
                  @foreach ($result as $row)
                <tr>
                  <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                  <td>{{ $row->nama_matakuliah }}</td>
                  <td>{{ $row->id_pengajar }}</td>
                  <td>
                    <a href="{{ url("matakuliah/$row->id_matakuliah/edit") }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                    <form action="{{ url("matakuliah/$row->id_matakuliah/delete") }}" method="POST" style="display:inline;">
                      {{ csrf_field()}}
                      {{ method_field('DELETE')}}
                      <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                    </form>
                    </td>>
                    </tr>
                    @endforeach
              </tbody>
            </table>>
        </div>
        <!-- /.box-footer-->
        </div>
        <!-- /.box-footer-->
    </section>
    <!-- /.content -->
    @endsection